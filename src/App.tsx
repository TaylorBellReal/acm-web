import TestPage from './components/TestPage'
import LetuHeader from './components/LetuHeader'
import Letufooter from './components/LetuFooter'

export default function App() {
    return (
        <>
            <LetuHeader />
            <Letufooter />
            <TestPage />
        </>
    )
}
